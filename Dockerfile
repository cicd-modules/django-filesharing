FROM almalinux/9-minimal

RUN microdnf update -y

RUN microdnf install -y python3 python3-pip

COPY * .

RUN pip3 install -r ./requirements.txt

EXPOSE 8000

CMD python3 manage.py runserver